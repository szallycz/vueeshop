import { defineStore } from 'pinia'
import type { Product } from '@/models/Product/product'
import { useStorage } from '@vueuse/core'
export const useProductStore = defineStore('product', {
  state: () => {
    return {
      shoppingCart: useStorage(
        'shopping-cart',
        {
          products: [] as Product[],
          numberOfProducts: 0,
          totalPrice: 0
        },
        localStorage,
        { mergeDefaults: true }
      )
    }
  },
  actions: {
    addProductToCart(item:Product) {
      const existingItem = this.shoppingCart.products.find((i) => i.id === item.id)
      if (existingItem) {
        

      } else {
        this.shoppingCart.products.push(item)
      }
      this.getTotalPrice()
      this.getNumberOfProducts()
    },
    getTotalPrice() {
      this.shoppingCart.totalPrice = 0
      this.shoppingCart.products.forEach((item) => {
        this.shoppingCart.totalPrice += item.price
      })
    },
    getNumberOfProducts() {
      return (this.shoppingCart.numberOfProducts = this.shoppingCart.products.length)
    },
    deleteProduct(id:Number) {
      const newArray = this.shoppingCart.products.filter((object) => {
        return object.id !== id
      })
      this.shoppingCart.products = newArray
      this.getTotalPrice()
      this.getNumberOfProducts()
    }
  },
  getters: {}
})
