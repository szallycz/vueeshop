import { useProductStore } from '@/stores/product'
import { useRouter } from 'vue-router'
import type { Product } from '@/models/Product/product'
export default function useProduct() {
  const productStore = useProductStore()
  const router = useRouter()

  const sendProduct = (product: Product) => {
    productStore.addProductToCart(product)
  }
  const openProduct = (id: number, title: string) => {
    router.push({ name: 'ProductPreview', params: { title }, query: { id: id } })
  }
  return {
    sendProduct,
    openProduct
  }
}
