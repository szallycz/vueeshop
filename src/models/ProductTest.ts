export interface Product {
  id: Number
  title: String
  price: Number
  description: String
  thumbnail: String
}
