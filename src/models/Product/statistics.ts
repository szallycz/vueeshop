interface StatisticInfo {
    id: number;
    name: string;
    description: string;
    displayName: string;
  }
  
export interface Statistic {
    id: number;
    statisticInfo: StatisticInfo;
    statisticValue: string;
  }
  