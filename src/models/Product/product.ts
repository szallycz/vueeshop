import type { Specification } from "./specification";
import type { Description } from "./description";
import type { Category } from "./category";
import type { Review } from "./review";
import type { Statistic } from "./statistics";
export interface Product {
    id: number;
    title: string;
    description: string;
    price: number;
    thumbnail:string;
    rating:number
    vat?: number;
    isAvailable?: boolean;
    isVisible?: boolean;
    stock: number;
    deliveryTime?: number;
    stockLevel?: number;
    descriptions?: Description[];
    created?: string;
    updated?: string;
    categories?: Category[];
    specifications?: Specification[];
    reviews?: Review[];
    statistics?: Statistic[];
  }
