export interface Review {
    id: number;
    userName: string;
    description: string;
    created: string;
    updated: string;
    message: string;
    rate: number;
    pros: string[];
    cons: string[];
    upVotes: number;
    downVotes: number;
    country: string;
    reportedCount: number;
    isVisible: boolean;
    isVerified: boolean;
  }
  