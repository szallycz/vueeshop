export interface Description {
  id: number;
  title: string;
  text: string;
  image: null | string;
  imageSource: null | string;
}
