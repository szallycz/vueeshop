interface SpecificationUnit {
    id: number;
    name: string;
    description: string;
    displayValue: string;
    variableType: number;
  }
  
  interface SpecificationInfo {
    id: number;
    name: string;
    unit: SpecificationUnit;
    description: string;
  }
  
  interface SpecificationCategory {
    id: number;
    name: string;
    displayValue: null | string;
    description: null | string;
  }
  
  export interface Specification {
    id: number;
    specificationInfo: SpecificationInfo;
    specificationValue: string;
    specificationCategory: SpecificationCategory;
  }