export interface Category {
    id: number;
    parentCategoryId: number;
    name: string;
    description: string;
    displayName: string;
  }